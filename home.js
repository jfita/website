(function() {
	'use strict';

	const figures = document.querySelectorAll('#games figure');
	const numFigures = figures.length;
	for(let i = 0; i < numFigures; i++) {
		const figure = figures[i];
		figure.addEventListener('mouseenter', showGif);
	}

	function showGif() {
		const image = this.querySelector('.animation');
		if ( ! image ) {
			return;
		}
		image.src = image.dataset.src;
		image.onload = () => {
			this.classList.add('gif');
			const loader = this.querySelector('.gifloader');
			if (loader) {
				loader.remove();
			}
		}
		this.removeEventListener('mouseenter', showGif);
	}

	if ( ! CSS.supports('(grid-template-rows: masonry)') ) {
		const js = document.createElement('script');
		js.src = 'masonry.pkgd.min.js';
		js.onload = () => {
			const list = document.querySelector('#games ul');
			if (!list) {
				return;
			}
			list.classList.add('masonry-fallback');
			new Masonry(list, {
				itemSelector: 'li',
				columnWidth: '.grid-sizer',
				percentPosition: true,
				gutter: 10
			});
		};
		document.body.append(js);
	}
})();
