<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:date="http://exslt.org/dates-and-times"
	extension-element-prefixes="date"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="common.xsl" />

	<xsl:variable name="base" select="'.'"/>

	<xsl:output
		method="html"
		version="1.0"
		encoding="UTF-8"
		omit-xml-declaration="yes"
		indent="yes"
		doctype-system="about:legacy-compat"
		/>

	<xsl:template match="home">
		<html lang="en">
			<head>
				<xsl:call-template name="meta">
					<xsl:with-param name="description" select="'My personal home page, showcasing games i have made.'"/>
				</xsl:call-template>
				<title>Perita</title>
				<link rel="stylesheet" type="text/css" media="screen" href="home.css" />
			</head>
			<body>
				<xsl:call-template name="header">
					<xsl:with-param name="page" select="'games'"/>
				</xsl:call-template>
				<main>
					<xsl:apply-templates/>
				</main>
				<xsl:call-template name="footer"/>
				<script src="home.js"></script>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="games">
		<section id="games">
			<h2>Games</h2>
			<ul>
				<xsl:apply-templates/>
				<li class="grid-sizer"></li>
			</ul>
		</section>
	</xsl:template>

	<xsl:template match="game">
		<li>
			<xsl:if test="@featured != ''">
				<xsl:attribute name="class">featured</xsl:attribute>
			</xsl:if>
			<a>
				<xsl:attribute name="href"><xsl:value-of select="concat(@slug, '/index.html')"/></xsl:attribute>
				<figure>
					<div class="gifloader"></div>
					<img class="static" alt="">
						<xsl:attribute name="src"><xsl:value-of select="concat(@slug, '/cover.png')"/></xsl:attribute>
						<xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute>
						<xsl:attribute name="height"><xsl:value-of select="@height"/></xsl:attribute>
					</img>
					<img class="animation" alt="">
						<xsl:attribute name="data-src"><xsl:value-of select="concat(@slug, '/cover.gif')"/></xsl:attribute>
						<xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute>
						<xsl:attribute name="height"><xsl:value-of select="@height"/></xsl:attribute>
					</img>
					<figcaption>
						<xsl:apply-templates select="document(concat(@slug, '/index.xml'))/game/info/productname"/>
						<xsl:apply-templates select="document(concat(@slug, '/index.xml'))/game/info/abstract"/>
					</figcaption>
				</figure>
			</a>
		</li>
	</xsl:template>

	<xsl:template match="productname">
		<span class="title"><xsl:apply-templates/></span>
	</xsl:template>

	<xsl:template match="abstract">
		<span class="description"><xsl:apply-templates/></span>
	</xsl:template>

	<xsl:template match="externalgame">
		<li>
			<a href="{@href}">
				<figure>
					<img src="{@cover}" width="{@width}" height="{@height}" alt=""/>
					<figcaption style="z-index: 10">
						<span class="title"><xsl:value-of select="@title"/></span>
						<span class="description"><xsl:value-of select="@abstract"/></span>
					</figcaption>
				</figure>
			</a>
		</li>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:apply-imports/>
	</xsl:template>

</xsl:stylesheet>
