(function() {
	'use strict';

	const runGameButton = document.getElementById('run-game');
	runGameButton.addEventListener('click', () => {
		const placeholder = document.getElementById('iframe-placeholder');
		const iframe = document.getElementById('iframe-template').content.firstChild;
		placeholder.replaceWith(iframe);
	});
})();
